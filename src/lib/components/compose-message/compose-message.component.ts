import {Component, Input, EventEmitter, Output, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import {ConfigurationService} from '@universis/common';
import * as Quill from 'quill';
import {QuillDeltaToHtmlConverter} from 'quill-delta-to-html';

@Component({
  selector: 'app-compose-message',
  templateUrl: './compose-message.component.html'
})
export class ComposeMessageComponent implements AfterViewInit {

  @Input() model!: { subject: string; body: string; attachments: any[] };
  @Input() showAttach = true;
  @Input() showButtons?: boolean = true;
  @Input() showHeader?: boolean = true;
  @Output() modelChange: EventEmitter<any> = new EventEmitter();
  public currentLang: any;
  @Output() submit: EventEmitter<any> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @ViewChild('messageBody') messageBody?: ElementRef;

  constructor(private _configurationService: ConfigurationService) { }

  ngAfterViewInit() {
    this.currentLang = this._configurationService.currentLocale;
    const QuillEditor: any = Quill;
    const messageBodyEditor = new QuillEditor(this.messageBody?.nativeElement, {
      theme: 'snow'
    });
    messageBodyEditor.on('text-change', (event) => {
      const contents: { ops: any[] } = messageBodyEditor.getContents();
      const converter = new QuillDeltaToHtmlConverter(contents.ops, {});
      this.model.body = converter.convert();
    });
  }

  onFileAdd(event: any) {
    const addedFile = event.addedFiles[0];
      this.model.attachments = [
      addedFile
    ];
  }

  onFileRemove(event: any) {
    this.model.attachments = [];
  }



}
