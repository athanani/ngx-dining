import { TestBed } from '@angular/core/testing';

import { DocumentSeriesResolverService } from './document-series-resolver.service';

describe('DocumentSeriesResolverService', () => {
  let service: DocumentSeriesResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocumentSeriesResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
