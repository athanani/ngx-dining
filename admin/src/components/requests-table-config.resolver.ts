import { TableConfiguration } from '@universis/ngx-tables';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';


const list_config_active = require('./dining-list.config.active.json');
const list_config_all = require('./dining-list.config.all.json');
const search_config_active = require('./dining-search.config.active.json');
const search_config_all = require('./dining-search.config.all.json');

export class RequestsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
       
        try {
            if (route.params.list == 'active') {
                return list_config_active;
            }
            else if (route.params.list == 'all') {
                return list_config_all;
            }
        } catch (err) {
            return list_config_all;
        }
    }
}

export class RequestsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
        try {
            if (route.params.list == 'active') {
                return search_config_active;
            }
            else if (route.params.list == 'all') {
                return search_config_all;
            }
        } catch (err) {
            return search_config_all;
        }
    }
}

export class RequestsDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return list_config_active;
    }
}