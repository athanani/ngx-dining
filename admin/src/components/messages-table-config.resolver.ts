import { TableConfiguration } from '@universis/ngx-tables';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';


import { MESSAGES_LIST_CONFIG_ALL } from './messages-list.config.all';
import { MESSAGES_SEARCH_CONFIG_ALL } from './messages-search.config.all';
import { MESSAGES_LIST_CONFIG_ACTIVE } from './messages-list.config.active'
import { MESSAGES_SEARCH_CONFIG_ACTIVE } from './messages-search.config.active'

export class MessagesTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
        try {
            if (route.params.list == 'active') {
                return MESSAGES_LIST_CONFIG_ACTIVE;
            }
            else if (route.params.list == 'all') {
                return MESSAGES_LIST_CONFIG_ALL;
            }
        } catch (err) {
            return MESSAGES_LIST_CONFIG_ACTIVE;
        }
    }
}

export class MessagesTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
        try {
            if (route.params.list == 'active') {
                return MESSAGES_SEARCH_CONFIG_ACTIVE;
            }
            else if (route.params.list == 'all') {
                return MESSAGES_SEARCH_CONFIG_ALL;
            }
        } catch (err) {
            return MESSAGES_SEARCH_CONFIG_ACTIVE;
        }
    }
}

export class MessagesDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return MESSAGES_LIST_CONFIG_ACTIVE;
    }
}