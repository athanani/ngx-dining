import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PreferenceHomeComponent } from './preference-home.component';

describe('PreferenceHomeComponent', () => {
  let component: PreferenceHomeComponent;
  let fixture: ComponentFixture<PreferenceHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PreferenceHomeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PreferenceHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
