export const MESSAGES_LIST_CONFIG_ACTIVE = {
  "$schema": "https://universis.gitlab.io/ngx-tables/schemas/v1/schema.json",
  "model": "DiningRequestActionMessages",
  "title": "UniversisDiningModule.Messages.Title",
  // tslint:disable-next-line: max-line-length
  "searchExpression": "(indexof(action/requestNumber, '${text}') ge 0 or indexof(student/uniqueIdentifier, '${text}') ge 0 startswith(sender/alternateName, '${text}') eq true)",
  "columns": [
    {
      "name": "action",
      "property": "action",
      "formatter": "ButtonFormatter",
      "className": "text-center",
      "formatOptions": {
        "buttonContent": "<i class=\"far fa-envelope text-indigo\"></i>",
        "buttonClass": "btn btn-default",
        "commands": [
          {
            "outlets": {
              "modal": ["item", "${action.id}", "edit"]
            }
          }
        ],
        "navigationExtras": {
          "fragment": "show-messages"
        }
      }
    },
    {
      "name": "sender/alternateName",
      "property": "senderName",
      "title": "UniversisDiningModule.Messages.Sender",
      "formatter": "TemplateFormatter",
      "formatString": "${senderName}"
    },
    {
      "name": "recipient/alternateName",
      "property": "sentByStudent",
      "title": "UniversisDiningModule.Messages.SentByStudent",
      "formatter": "TemplateFormatter",
      "formatString": "${sentByStudent}",
      "hidden": true
    },
    {
      "name": "recipient/alternateName",
      "property": "recipientName",
      "title": "UniversisDiningModule.Messages.Recipient",
      "formatter": "TemplateFormatter",
      "formatString": "${recipientName}",
      "hidden": true
    },
    {
      "name": "subject",
      "property": "subject",
      "title": "UniversisDiningModule.Messages.Subject",
      "formatter": "TemplateFormatter",
      "formatString": "${subject}"
    },
    {
      "name": "action/requestNumber",
      "property": "requestNumber",
      "title": "Requests.RequestNumber",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${requestNumber === code ? null : requestNumber}"
        }
      ]
    },
    {
      "name": "action/actionStatus/alternateName",
      "property": "actionStatus",
      "title": "Requests.ActionStatusTitle",
      "formatters": [
        {
          "formatter": "TranslationFormatter",
          "formatString": "ActionStatusTypes.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${actionStatus}'==='CancelledActionStatus' || '${actionStatus}'==='FailedActionStatus'",
              "text-warning": "'${actionStatus}'==='ActiveActionStatus'",
              "text-success": "'${actionStatus}'==='CompletedActionStatus'",
              "text-primary": "'${actionStatus}'==='PotentialActionStatus'",
            }
          }
        }
      ]
    },
    {
      "name": "action/effectiveStatus",
      "property": "effectiveStatus",
      "title": "UniversisDiningModule.EffectiveStatus",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${effectiveStatusState ? action.effectiveStatus.locale.name : ''}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-success": "'${effectiveStatusState}' === 'AcceptedAttachmentsEffectiveStatus'",
              "text-danger": "'${effectiveStatusState}' != 'AcceptedAttachmentsEffectiveStatus'"
            }
          }
        }
      ]
    },
    {
      "name": "action/effectiveStatus/alternateName",
      "property": "effectiveStatusState",
      "title": "effectiveStatus",
      "hidden": true
    },
    {
      "name": "action/agent/name",
      "property": "agent",
      "title": "UniversisDiningModule.Agent.claim"
    },
    {
      "name": "student/studentStatus/alternateName",
      "property": "studentStatus",
      "title": "Requests.StudentStatus",
      "formatter": "TranslationFormatter",
      "formatString": "StudentStatuses.${value}"
    },
    {
      "name": "student/department/name",
      "property": "studentDepartment",
      "title": "Students.Department",
    },
    {
      "name": "dateCreated",
      "property": "dateCreated",
      "title": "UniversisDiningModule.Date",
      "formatter": "DateTimeFormatter",
      "formatString": "short"
    },
    {
      "name": "action/code",
      "property": "code",
      "title": "Requests.Code",
      "hidden": true
    }
  ],
  "defaults": {
    "filter": "(action/diningRequestEvent/eventStatus/alternateName eq 'EventOpened')",
    "expand": "sender,recipient,action($expand=effectiveStatus($expand=locale))",
    "orderBy": "dateCreated desc"
  },
  "paths": [
    {
      "name": "UniversisDiningModule.Messages.Active",
      "show": true,
      "alternateName": "list/active"
    },
    {
      "name": "UniversisDiningModule.Messages.All",
      "show": true,
      "alternateName": "list/all"
    }
  ],
  "criteria": [
    {
      "name": "senderName",
      "filter": "(indexof(sender/alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "recipientName",
      "filter": "(indexof(recipient/alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "sentByStudent",
      "filter": "(indexof(recipient/alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "actionStatus",
      "filter": "(action/actionStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "effectiveStatusState",
      "filter": "(action/effectiveStatus/alternateName eq '${value}')",
      "type": "text"
    },
    {
      "name": "studentUniqueIdentifier",
      "filter": "(indexof(student/uniqueIdentifier, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "requestNumber",
      "filter": "(indexof(action/requestNumber, '${value}') ge 0)",
      "type": "text",
    },
    {
      "name": "agent",
      "filter": "(action/agent/name eq '${value}')",
      "type": "text"
    },
    {
      "name": "studentDepartment",
      "filter": "(student/department/id eq '${value}')",
      "type": "text"
    },
    {
      "name": "minDate",
      "filter": "(dateCreated ge '${new Date(value).toISOString()}')"
    },
    {
      "name": "maxDate",
      "filter": "(dateCreated le '${new Date(value).toISOString()}')"
    },
    {
      "name": "studentActiveStatus",
      "filter": "${value >=1 ? '(student/studentStatus/alternateName eq \\'active\\')' : '(student/studentStatus/alternateName ne \\'active\\')'}",
      "type": "text"
    }
  ],
  "searches": []
};
