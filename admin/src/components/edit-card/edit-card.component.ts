import { AfterViewInit, Component, Input, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { DataServiceQueryParams } from '@themost/client';
import { ModalService, LoadingService, ErrorService, AppEventService } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'lib-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.scss', '../../../../src/lib/components/dining/dining.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditCardComponent implements OnInit, AfterViewInit {
  public static readonly ServiceQueryParams = {
    $expand: 'student($expand=person,studentStatus,inscriptionMode,department,studyProgram($expand=studyLevel)), action($expand=agent,review,effectiveStatus,actionStatus, modifiedBy, category)'
  };

  dataSubscription: any;
  paramSubscription: any;
  editingReview: any = false;
  @ViewChild('form') form?: AdvancedFormComponent;
  @Input() model: any;
  @Input() showNavigation = true;
  @Input() showActions = true;
  public modalRef!: BsModalRef;
  cancelReason: any = null;

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _router: Router,
    private _modal: ModalService,
    private _modalService: BsModalService,
    private _loading: LoadingService,
    private _errorService: ErrorService,
    private _translateService: TranslateService,
    private _appEvent: AppEventService) { }


  ngOnInit() {
    //
  }

  ngAfterViewInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe((query) => {
      //
    });
    this.dataSubscription = this._activatedRoute.data.subscribe((data) => {
      this.model = data['model'];
    });
  }

  refreshForm(){
    if (this.form) {
      this.form.refreshForm.emit({
        submission: {
          data: this.model
        }
      });
    }
  }

  reload() {
    this._loading.showLoading();
    if (this.model == null) {
      // do nothing
      return;
    }
    return this._context.model('StudentDiningCards')
      .asQueryable(<DataServiceQueryParams>EditCardComponent.ServiceQueryParams)
      .where('id').equal(this.model.id)
      .getItem().then((result) => {
        this.model = result;
        this.refreshForm();
        this._loading.hideLoading();
      });
  }


  cancel(templateToLoad: TemplateRef<any>) {
    this.modalRef = this._modalService.show(templateToLoad);
  }

  reactivate(templateToLoad: TemplateRef<any>) {
    this.modalRef = this._modalService.show(templateToLoad);
  }

  // confirm callback of the cancel card modal 
  // user accepted to cancel dining card => reject active state(true), reset cancelReason and close modal
  public confirmCancelCardModal(cancelReason: string, active: boolean) {
    if (cancelReason) {
      this._loading.showLoading();
      this.modalRef.hide();
      // cancel dining card
      this.model.active = active;
      // set today as dateCancelled
      this.model.dateCancelled = this.model.validThrough = new Date;
      this.model.cancelReason = cancelReason;
      return this._context.model('StudentDiningCards').save(this.model)
        .then(() => {
          // reload page
          return this.reload().then(() => {
            // send an application event
            this._appEvent.change.next({
              model: 'StudentDiningCards',
              target: this.model
            });
            this.cancelReason = null;
          }).catch((err) => {
            this._loading.hideLoading();
            // show error dialog
            this._modal.showErrorDialog(this._translateService.instant('Register.ReloadError.Title'),
            this._translateService.instant('Register.ReloadError.Message')).then(r => {
              this._router.navigate(['/dining/dining-cards']);
            });
          });
        }, (err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    } else {
      // User did not insert any text
      alert(this._translateService.instant('UniversisDiningModule.CancelCardModal.TitleOnUserError'));
    }
  }

  confirmCancelCardActiveRequestModal(cancelReason: string){
    if (cancelReason) {
      this._loading.showLoading();
      this.modalRef.hide();
      // cancel dining card
      return this._context.model(`StudentDiningCards/${this.model.id}/Cancel`).save({
        cancelReason: cancelReason
      })
        .then(() => {
          // reload page
          return this.reload().then(() => {
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'StudentDiningCards',
              target: this.model
            });
            this.cancelReason = null;
          }).catch((err) => {
            this._loading.hideLoading();
            // show error dialog
            this._modal.showErrorDialog(this._translateService.instant('Register.ReloadError.Title'),
            this._translateService.instant('Register.ReloadError.Message')).then(r => {
              this._router.navigate(['/dining/dining-cards']);
            });
          });
        }, (err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    } else {
      // User did not insert any text
      alert(this._translateService.instant('UniversisDiningModule.CancelCardModal.TitleOnUserError'));
    }
  }

  public confirmReactivateCardModal(active: boolean) {
    this.model.active = active;
    this.checkDates();
    this.modalRef.hide();
    // cancel dining card
    this._loading.showLoading();
    return this._context.model('StudentDiningCards').save(this.model).then(async (result) => {
      this.model = result;
      // reload page
      return this.reload().then(() => {
        // send an application event
        this._appEvent.change.next({
          model: 'StudentDiningCards',
          target: this.model
        });
      }).catch((err) => {
        this._loading.hideLoading();
        // show error dialog
        this._modal.showErrorDialog(this._translateService.instant('Register.ReloadError.Title'),
        this._translateService.instant('Register.ReloadError.Message')).then(r => {
          this._router.navigate(['/dining/dining-cards']);
        });
      });
    }, (err) => {
      this._loading.hideLoading();
      // clear
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  checkDates(){
    if (typeof this.model.dateCancelled !== 'undefined' && this.model.dateCancelled != null) {
      this.model.dateCancelled = null;
    }
    if (typeof this.model.cancelReason != 'undefined' && this.model.cancelReason != null) {
      this.model.cancelReason = null;
    }
  }

}
