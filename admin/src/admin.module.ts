import { CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AdvancedFormsModule } from '@universis/forms';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NgArrayPipesModule } from 'ngx-pipes';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { SharedModule } from '@universis/common';
import { AdminDiningRoutingModule } from './admin-routing.module';
import { TablesModule } from '@universis/ngx-tables';
import { ModalEditComponent } from './components/edit/modal-edit.component';
import { EditComponent } from './components/edit/edit.component';
import { ComposeMessageComponent } from './components/compose-message/compose-message.component';
import { SidePreviewComponent } from './components/side-preview/side-preview.component';
import { AttachmentDownloadComponent } from './components/attachment-download/attachment-download.component';
import { ListComponent } from './components/list/list.component';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { ModalEditCardComponent } from './components/edit-card/modal-edit-card/modal-edit-card.component';
import { RouterModalModule } from '@universis/common/routing';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { ReviewComponent } from './components/review/review.component';
import { CardSidePreviewComponent } from './components/card-side-preview/card-side-preview.component';
import { CardsListComponent } from './components/cards-list/cards-list.component';
import { ReportService, ReportsSharedModule } from '@universis/ngx-reports';
import { NgxSignerModule } from '@universis/ngx-signer';
import { DocumentSeriesResolverService } from './document-series-resolver.service';
import { MessagesListComponent } from './components/messages-list/messages-list.component';
import { CardsHomeComponent } from './components/cards-home/cards-home.component';
import { RequestsHomeComponent } from './components/requests-home/requests-home.component';
import { RequestsDefaultTableConfigurationResolver, RequestsTableConfigurationResolver, RequestsTableSearchResolver } from './components/requests-table-config.resolver';
import { CardsDefaultTableConfigurationResolver, CardsTableConfigurationResolver, CardsTableSearchResolver } from './components/cards-table-config.resolver';
import { PreferenceListComponent } from './components/preference-list/preference-list.component';
import { PreferenceHomeComponent } from './components/preference-home/preference-home.component';
import { PreferenceTableConfigurationResolver, PreferenceTableSearchResolver } from './components/preference-table-config.resolver';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { MessagesHomeComponent } from './components/messages-home/messages-home.component';
import { MessagesTableConfigurationResolver, MessagesTableSearchResolver } from './components/messages-table-config.resolver';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    AdminDiningRoutingModule,
    AdvancedFormsModule,
    MostModule,
    SharedModule,
    RouterModalModule,
    TranslateModule,
    NgArrayPipesModule,
    NgxDropzoneModule,
    TablesModule,
    ProgressbarModule, 
    ReportsSharedModule,
    NgxSignerModule, 
    NgxExtendedPdfViewerModule
  ],
  declarations: [
    ModalEditComponent,
    EditComponent,
    ComposeMessageComponent,
    SidePreviewComponent,
    AttachmentDownloadComponent,
    ListComponent,
    EditCardComponent,
    ModalEditCardComponent,
    ReviewComponent,
    CardSidePreviewComponent, 
    CardsListComponent, 
    MessagesListComponent, 
    CardsHomeComponent,
    RequestsHomeComponent,
    PreferenceListComponent,
    PreferenceHomeComponent,
    MessagesHomeComponent
  ],
  exports: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    ReportService, 
    DocumentSeriesResolverService,
    RequestsTableConfigurationResolver,
    RequestsTableSearchResolver,
    RequestsDefaultTableConfigurationResolver, 
    CardsTableConfigurationResolver, 
    CardsTableSearchResolver,
    CardsDefaultTableConfigurationResolver, 
    PreferenceTableConfigurationResolver,
    PreferenceTableSearchResolver, 
    MessagesTableConfigurationResolver, 
    MessagesTableSearchResolver
  ]
})
export class AdminDiningModule {  
}
