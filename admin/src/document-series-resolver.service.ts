import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';

@Injectable()
export class DocumentSeriesResolverService implements Resolve<any>{

    constructor() {
    }
    resolve(): Promise<any> | any {
      const url = 'InstituteDocumentNumberSeries?$select=id,name&$orderby=name&$filter=active eq true';
      return url;
    }
  }
