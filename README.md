# @universis/ngx-dining

Client library of [`@universis/dining`](https://gitlab.com/universis/dining) module.

# Usage

    npm i @universis/dining

# Configure

`@universis/ngx-dining` comes with a collection of assets which should be included in application assets

        # angular.json

        "architect": {
            "build": {
            "builder": "@angular-devkit/build-angular:browser",
            "options": {
                "outputPath": "dist",
                "index": "src/index.html",
                "main": "src/main.ts",
                "polyfills": "src/polyfills.ts",
                "tsConfig": "src/tsconfig.app.json",
                "preserveSymlinks": true,
                "assets": [
                    ...
                    {
                        "glob": ".",
                        "input": "node_modules/@universis/ngx-dining/assets",
                        "output": "assets"
                    }
                ],
                ...
