import { el } from './dining.el';
import { en } from './dining.en';

export const DINING_LOCALES = {
  el,
  en
};
